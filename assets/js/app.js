$(document).ready(function() {

  //run animations onload Home;
  var firstSlide = $('#slider .swiper-slide:nth-child(1)');
  setTimeout(function() {
    firstSlide.find('.animated').css({
      'display': 'block',
    });
  }, 2000);

  //stycky
  if (!$('.hidden-menu').length) {
    $('body').append('<div class="hidden-menu" style="display:none;"><div class="in-frame"></div></div>');
    $('.hidden-menu .in-frame').html($('.header-bottom').clone());
  }
  $(document).scroll(function() {
    if ($(document).scrollTop() >= 100) {
      $('.hidden-menu').fadeIn();
    }
    if ($(document).scrollTop() <= 100) {
      $('.hidden-menu').fadeOut();
    }
  });

  //Mobile Navigation
  if ($('.mobile-navigation').length) {
    $('.mobile-navigation').html($('#site-navigation').clone());
    $('.mobile-navigation #site-navigation > ul').prepend('<li><a href="http://resguarda.com">HOME</a><li>');
    $('.mobile-navigation').find('.dropdown').parent().find('a').addClass('trigger');
    $('.mobile-navigation').find('.dropdown').parent().addClass('caret-holder-down');
    $('.nav-toggle').click(function() {
      if ($('.mobile-navigation').css('display') === 'block') {
        $('.mobile-navigation').slideUp('fast');
      } else {
        $('.mobile-navigation').slideDown('fast');
      }
    });
    $('.trigger').click(function(e) {
      e.preventDefault();
      var $pr = $(this).parent(),
        $drop = $pr.find('.dropdown');
      if ($drop.css('display') === 'block') {
        $('.mobile-navigation').find('.dropdown').slideUp('fast');
        $drop.parent().removeClass('caret-holder-up');
        $drop.parent().addClass('caret-holder-down');
      } else {
        $drop.slideDown('fast');
        $drop.parent().removeClass('caret-holder-down');
        $drop.parent().addClass('caret-holder-up');
        $('.mobile-navigation').find('.dropdown').not($drop).slideUp('fast');
      }
      $('.mobile-navigation').find('.dropdown').not($drop).parent().removeClass('caret-holder-up');
      $('.mobile-navigation').find('.dropdown').not($drop).parent().addClass('caret-holder-down');
    });
  }

  //add data information to inline-style;
  $('.animated').each(function() {
    var self = this;
    $(this).css({
      'animation-delay': $(this).attr('data-delay-animation') / 1000 + 's',
      'animation-duration': $(this).attr('data-duration-animation') / 1000 + 's',
    });
  });

  //onScreen
  $('section#que-hacemos ,section#beneficios,section#denuncias,section#video,section#denuncias-interno,section#compliance,section#fraudes,section#investigacion').onScreen({
    container: window,
    direction: 'vertical',
    doIn: function() {
      var els = $('.onScreen').not('#slider').find('.animated');
      //clone and delete caption elements to rerun animations (firefox bug)
      els.each(function() {
        if (!$(this).attr('data-nowone')) {
          var el = $(this);
          var newone = el.clone(true);
          el.before(newone);
          $("." + el.attr("class") + ":last");
          if (el.hasClass("inlineblock")) {
            newone.css({
              'display': 'inline-block'
            });
          } else {
            newone.css({
              'display': 'block'
            });
          };
          newone.attr('data-nowone', 1);
          el.remove();
        }
      });
    },
    doOut: function() {
    },
    tolerance: 300,
    // throttle: 50,
    toggleClass: 'onScreen',
    lazyAttr: null,
    //lazyPlaceholder: 'someImage.jpg',
    debug: false
  });
  //SliderHome
  if ($('#slider').length) {
    var elSlider = new Swiper('.swiper-container', {
      // Optional parameters
      direction: 'horizontal',
      loop: true,
      autoplay: 10000,
      runCallbacksOnInit: true,
      paginationClickable: true,
      pagination: '.swiper-pagination',
      nextButton: '.swiper-button-next',
      prevButton: '.swiper-button-prev',
      //simulateTouch: false,
      //scrollbar: '.swiper-scrollbar',
    });
    elSlider.on('slideChangeStart', function(swiper) {
      setTimeout(function() {
        $('#slider .animated').hide();
      }, 0);
    });
    elSlider.on('slideChangeEnd', function(swiper) {
      var els = $('#slider .swiper-slide-active').find('.animated');
      //clone and delete caption elements to rerun animations (firefox bug)
      els.each(function() {
        var el = $(this);
        var newone = el.clone(true);
        el.before(newone);
        //$("." + el.attr("class") + ":last");
        newone.css({
          'display': 'block'
        });
        el.remove();
      });
    });
  }

  //Agenda Navigation
  $('.agenda-widget .agenda-next,.agenda-widget.agenda-prev').click(function() {
    $('.agenda-content').html('<div class="preloader"></div>')
  })
});

if ($('#novedades-in').length) {
  var contentSlider = new Swiper('.swiper-container', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
  });
}
if ($('#slider-clientes').length) {
  var clientSlider = new Swiper('.swiper-container', {
    loop: true,
    paginationClickable: true,
    pagination: '.swiper-pagination',
  });
  clientSlider.on('slideChangeStart', function(swiper) {
    setTimeout(function() {
      $('#slider-clientes .animated').hide();
    }, 0);
  });
  clientSlider.on('slideChangeEnd', function(swiper) {
    var els = $('#slider-clientes .swiper-slide-active figure').find('.animated');
    //clone and delete caption elements to rerun animations (firefox bug)
    //  console.log(els);
    els.each(function() {
      var el = $(this);
      var newone = el.clone(true);
      el.before(newone);
      //  $("." + el.attr("class") + ":last");
      newone.css({
        'display': 'block'
      });
      el.remove();
    });
  });


}
